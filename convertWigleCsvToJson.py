#!/usr/bin/python
import csv
import json

with open ('wigle.csv', 'rb') as csvFile:
    reader = csv.DictReader(csvFile)
    rows = list(reader)

with open ('coordinates.js', 'w') as jsonFile:
    jsonFile.write("var ssidLocations = ")
    json.dump(rows, jsonFile)
    jsonFile.write(";")
