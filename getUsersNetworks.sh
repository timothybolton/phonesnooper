#!/bin/bash
if [[ -z "${1// }" ]]; then
    echo "You need to supply a value"
else
    cat /tmp/ssids.tmp | grep -i $1 | cut -d'=' -f5 | sort -u > relevantSsids.csv
fi
