#!/usr/bin/python
import csv
import json

with open ('relevantSsids.csv', 'rb') as csvFile:
    reader = csv.reader(csvFile)
    rows = list(reader)

with open ('relevantSsids.js', 'w') as jsonFile:
    jsonFile.write("var relevantSsids = ")
    json.dump(rows, jsonFile)
    jsonFile.write(";\nvar relevantSsids = [].concat.apply([],relevantSsids);")

