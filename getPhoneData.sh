#!/bin/bash
# NOTE: This REQUIRES a wireless card that can go into monitoring mode
# Look at the Alfa USB cards from Amazon.
WIRELESS_INTERFACE="wlan0"

ifconfig $WIRELESS_INTERFACE down
iwconfig $WIRELESS_INTERFACE mode monitor
ifconfig $WIRELESS_INTERFACE up
tshark -i $WIRELESS_INTERFACE -n -l subtype probereq | tee -a /tmp/ssids.tmp
